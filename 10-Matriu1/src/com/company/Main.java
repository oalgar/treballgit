package com.company;

import com.sun.media.jfxmediaimpl.HostUtils;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Random rnd = new Random();
    static Scanner scan = new Scanner(System.in);
    static String blau ="\33[9;34m";
    static String nc ="\33[0m";

    static int[][] crear_matriu(int maxf, int maxc) {
        int i, j;
        int[][] m = new int[maxf][maxc];

        for (i = 0; i < maxf; i++) {
            for (j = 0; j < maxc; j++) {

                m[i][j] = rnd.nextInt(10);
            }
        }

        return m;
    }


    static void imprimir_matriu(int[][] m) {
        int i, j;

        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }

    }

    static boolean buscaNum(int[][] m, int n) {
        int i, j;
        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m[i].length; j++) {


                if (n == m[i][j])
                    return true;

            }
        }
        return false;
    }

    private static void imprimir_matriu1(int[][] matriu, int n) {

        int i, j;

        for (i = 0; i < matriu.length; i++) {
            for (j = 0; j < matriu[i].length; j++) {
                if (matriu[i][j] == n) {
                    System.out.print(" "+ blau +matriu[i][j] + " " + nc);
                    n = 900;
                } else {
                    System.out.print(" - ");
                }


            }
            System.out.println();

        }

    }
    public static void main(String[] args) {

        int maxF=10,maxC=10;
        int[][] matriu;
        int num;
        boolean trobat;

        System.out.println("Dona'm el número a buscar\n");
        num = scan.nextInt();
        matriu = crear_matriu(maxF,maxC);
        imprimir_matriu(matriu);
        trobat = buscaNum(matriu, num);

        if (trobat) {
            System.out.println("\nEL número " + num + " SI ésta a la matriu\n");
            imprimir_matriu1(matriu, num);
        } else {
            System.out.println("\nEL número " + num + " NO ésta a la matriu\n");

        }

    }
}



