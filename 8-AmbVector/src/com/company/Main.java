package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Random rnd = new Random();
    static Scanner scan = new Scanner(System.in);

    static int[] crea_vector (int max) {
        int i;
        int [] v = new int[max];
        for (i = 0; i < max; i++){
            v[i] = rnd.nextInt(max);
        }
        return v;
    }

    static void imprimeix_vector( int[] v) {
        int i;
        for (i = 0; i < v.length; i++){
            System.out.println("v[" + i + "] = " + v[i]);
        }
    }

    public static void main(String[] args) {

        int max;
        System.out.println("Quina grandària té el vector?\n");
        max = scan.nextInt();
        int[] v;

        v = crea_vector(max);
        imprimeix_vector(v);
    }
}
