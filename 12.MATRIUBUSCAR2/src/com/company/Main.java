package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Random rnd = new Random();
    static Scanner scan = new Scanner(System.in);

    static int[][] crear_matriu(int max) {
        int i, j;
        int[][] m = new int[max][max];

        for (i = 0; i < max; i++) {
            for (j = 0; j < max; j++) {
                m[i][j] = rnd.nextInt(10);
            }
        }

        return m;
    }

    static void imprimir_matriu(int[][] m) {
        int i, j;

        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m.length; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }

    }



    public static void main(String[] args) {
        int max;

        System.out.println("Dona'm un número?\n");
        max = scan.nextInt();

        int[][] matriu;
        matriu = crear_matriu(max);
        imprimir_matriu(matriu);
    }
}
