package com.company;

import java.util.Random;

public class Main {
    static Random rnd = new Random();
    static String blau="\033[0;34m";
    static String verd="\033[0;32m";
    static String roig="\033[0;31m";
    static String nc="\033[0m";

    static int[][] crear_matriu(int max) {
        int i, j;
        int[][] m = new int[max][max];

        for (i = 0; i < max; i++) {
            for (j = 0; j < max; j++) {
                m[i][j] = rnd.nextInt(10);
            }
        }

        return m;
    }

    static void imprimir_matriu(int[][] m) {
        int i, j;


        for (i = 0; i < m.length; i++) {
            for (j = 0; j < m.length; j++) {


                if (m[i][j] % 2 == 0)
                    System.out.print("" + blau + "" + m[i][j] + " ");
                else
                    System.out.print("" + verd + "" + m[i][j] + " ");


            }System.out.println();
        }

    }

    public static void main(String[] args) {



        int[][] matriu;
        int max = 10;
        System.out.println("\t\t"+roig+"MATRIU"+nc);
        System.out.println("\t\t"+verd+"------");
        matriu = crear_matriu(max);
        imprimir_matriu(matriu);

    }
}
