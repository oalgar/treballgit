package PacketPrincipal;

import java.util.Scanner;

import static packet1.Hola.modifica_variable_global;
import static packet1.Hola.saluda;
import static packet2.Adeu.consulta_variable_global;
import static packet2.Adeu.despedeix;

public class Main {

    public static int numero; //creem una variable global
    public static Scanner scan= new Scanner(System.in);

    public static void main(String[] args) {


	String nom;
        System.out.println("Dona'm el teu nom");
        nom=scan.nextLine();

        //Tractament de funcions en altres paquets
        saluda(nom);
        despedeix(nom);

        // Trac

          modifica_variable_global();
          consulta_variable_global();


    }
}
