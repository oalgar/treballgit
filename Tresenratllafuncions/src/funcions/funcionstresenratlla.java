package funcions;

import static PacketPrincipal.Main.scan;

public class funcionstresenratlla {

    public static char[][] crearTaulell(int max) {

        char[][] quadre = new char[max][max];

        for (int i = 0; i < max; i++) {
            for (int j = 0; j < max; j++) {
                quadre[i][j] = '.';
            }
        }

        return quadre;
    }

    public static char[][] ferTirada(char [][] quadre, char fitxa) {

        int fila;
        int columna;

        System.out.println("\tEscull posició. Fila/Columna (entre 0 i 2):");

        do {
            do {
                System.out.println("\tEscull la fila (entre 0 i 2):");

                fila = scan.nextInt();
            } while (fila < 0 || fila > 2);
            do {
                System.out.println("\tEscull la columna (entre 0 i 2):");
                columna = scan.nextInt();
            } while (columna < 0 || columna > 2);


        } while (quadre[fila][columna] != '.');
        quadre[fila][columna]=fitxa;

        return quadre;
    }

    public static char escullFitxa(char fitxa) {


            if (fitxa == 'X') {
                fitxa = 'O';
            } else {
                fitxa = 'X';
            }


        return fitxa;

    }

    public static void imprimirTaulell(char[][] quadre, char fitxa) {


        System.out.println("TORN DEL JUGADOR "+fitxa);
        for (int i = 0; i < quadre.length; i++) {
            System.out.print("\n");
            for (int j = 0; j < quadre[i].length; j++) {
                System.out.print("\t " + quadre[i][j]);
            }
            System.out.print("\n\n\n");
        }

    }

    public static boolean comprovaGuanyar (char[][] quadre) {

        // files
        // columnes
        // Diagonal


        return true;
    }
        //jugador 2
}
