package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static Random rnd = new Random();
    static Scanner scan = new Scanner(System.in);

    static int[] crea_vector(int max) {
        int i;
        int[] v = new int[max];
        for (i = 0; i < max; i++) {
            v[i] = rnd.nextInt(max);
        }
        return v;
    }

    static void imprimeix_vector(int[] v) {
        int i;
        for (i = 0; i < v.length; i++) {
            System.out.println("v[" + i + "] = " + v[i]);
        }
    }

    static boolean buscaNum(int [] v, int num) {
        int i;
        for (i = 0; i < v.length; i++) {
            if ( num == v[i]) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {

        int max, num;
        boolean trobat;
        System.out.println("Quina grandària té el vector?\n");
        max = scan.nextInt();
        int[] v;

        v = crea_vector(max);
        imprimeix_vector(v);
        System.out.println("Dona'm el número a buscar\n");
        num = scan.nextInt();

        trobat = buscaNum(v, num);

        if (trobat) {
            System.out.println("EL número " + num + " SI ésta en el vector");
        } else {
            System.out.println("EL número " + num + " NO ésta en el vector");
        }
    }
}
