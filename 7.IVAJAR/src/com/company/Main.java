package com.company;

import java.util.Scanner;

public class Main {



    private static double formulaiva(double a, double b) {

        double result;
        result = (a * (100 + b)) / 100;
        return result;
    }
    public static void main(String[] args) {
	// write your code here
        double x, i, iva;

        if (args.length!=2) {

            System.out.println("Només dos valors");

        }
        else {
            x = Integer.parseInt(args[0]);
            i = Integer.parseInt(args[1]);

            iva = formulaiva(x, i);

            System.out.println("El " + i + "%  d'IVA de " + x + " és " + iva+ " €");
        }
    }
}
