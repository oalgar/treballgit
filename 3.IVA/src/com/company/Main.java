package com.company;

import java.util.Scanner;

public class Main {

    private static Scanner scan = new Scanner(System.in);

    private static double formulaiva(double a, double b) {

        double result;
        result = (a * (100 + b)) / 100;
        return result;
    }


    public static void main(String[] args) {
        double x, i, iva;
        System.out.println("Dona'm el preu");
        x = scan.nextDouble();
        System.out.println("Dona'm l'iva");
        i = scan.nextDouble();
        iva = formulaiva(x, i);

        System.out.println("El preu total és: " + iva + " €");


    }
}
