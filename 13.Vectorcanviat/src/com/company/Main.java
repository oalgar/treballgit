package com.company;

import java.util.Random;

public class Main {
    static Random rnd = new Random();

    private static int[] creaVector(int max) {

        int i;
        int [] v = new int[max];
        for (i = 0; i < max; i++){
            v[i] = rnd.nextInt(max);
        }
        return v;
    }

    private static void imprimeixvector (int [] v) {

        int i;
        for (i = 0; i < v.length; i++) {
            System.out.print(" , " + v[i]);
        }

    }

    private static int[] borrarParells(int[]v){

        int i;

        for (i = 0; i < v.length; i++){
            if (v[i] %2==0) {
                v[i] = rnd.nextInt(v.length);
                i--;
            }

        }
        return v;
    }


    public static void main(String[] args) {

        int max =20;
        int[]vect;
        int[]vect1;

        vect= creaVector(max);

        System.out.println("\nVector original\n");
        imprimeixvector(vect);

        vect1 = borrarParells(vect);

        System.out.println("\n\nVector Canviat\n");
        imprimeixvector(vect1);



    }


}
