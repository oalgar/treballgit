package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        int p;
        double a, b, total;

        //<editor-fold defaultstate ="collapsed" desc="Funcions ... ">
        //</editor-fold

        do {
            p = Integer.parseInt(JOptionPane.showInputDialog("Calculadora\n-----------------\n[1] Sumar\n[2] Restar\n[3] Multiplicar\n[4] Dividir\n" +
                    "[5] Potencia\n[6] Arrel Cuadrada\n[7] Sinus\n[8] Cosinus\n[9] Sortir\n"));

            switch (p) {

                case (1):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    b = Double.parseDouble(JOptionPane.showInputDialog("Dona'm altre número"));
                    total = a + b;
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (2):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    b = Double.parseDouble(JOptionPane.showInputDialog("Dona'm altre número"));
                    total = a - b;
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (3):
                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    b = Double.parseDouble(JOptionPane.showInputDialog("Dona'm altre número"));
                    total = a * b;
                    JOptionPane.showMessageDialog(null, total);
                    break;

                case (4):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    b = Double.parseDouble(JOptionPane.showInputDialog("Dona'm altre número"));
                    total = a / b;
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (5):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    b = Double.parseDouble(JOptionPane.showInputDialog("Dona'm altre número"));
                    total = Math.pow(a, b);
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (6):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    total = Math.sqrt(a);
                    JOptionPane.showMessageDialog(null, total);


                    break;

                case (7):

                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    total = Math.sin(a);
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (8):
                    a = Double.parseDouble(JOptionPane.showInputDialog("Dona'm un número"));
                    total = Math.cos(a);
                    JOptionPane.showMessageDialog(null, total);

                    break;

                case (9):

                    JOptionPane.showMessageDialog(null, "Moltes gràcies per la vostra consulta");

                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Has d'escollir entre 1 i 9");

            }
        } while (p != 9);
    }
}